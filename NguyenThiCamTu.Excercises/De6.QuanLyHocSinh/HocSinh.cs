﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De6.QuanLyHocSinh
{
    internal class HocSinh
    {
        public string MaHs { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }

        public HocSinh () { 
        }

        public HocSinh(string maHs, string ten, int tuoi)
        {
            this.MaHs = maHs;
            this.Ten = ten;
            this.Tuoi = tuoi;
        }

        public void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10}", MaHs, Ten, Tuoi);
        }
    }

}
