﻿using De6.QuanLyHocSinh;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Học Sinh: ");
    Console.WriteLine("1. Nhập Danh sách Học Sinh");
    Console.WriteLine("2. Xuất Danh sách Học Sinh Không Có Năm Sinh");
    Console.WriteLine("3. Xuất Danh sách Học Sinh Có Năm Sinh");
    Console.WriteLine("4. Xóa Học Sinh theo Mã Học Sinh");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 4);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachHocSinh();
            break;

        case 2:
            service.XuatDanhSachHocSinh();
            break;

        case 3:
            service.XuatDanhSachHocSinhCoNamSinh();
            break;

        case 4:
            service.XoaHocSinhTheoMa();
            break;
        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


