﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De6.QuanLyHocSinh
{
    internal class SERVICE
    {
        List<HocSinh> danhSachHocSinh = new List<HocSinh>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách sinh viên
        //public void DuLieuBanDau()
        //{
        //    danhSachHocSinh.Add(new HocSinhCoNamSinh { MaHs = "HE176697", Ten = "Nguyễn Thị Cẩm Tú",Tuoi = 20, NamSinh = 2003 });
        //    danhSachHocSinh.Add(new HocSinhCoNamSinh { MaHs = "HE172659", Ten = "Trương Thị Thanh Hòa", Tuoi = 21, NamSinh = 2002 });
        //    danhSachHocSinh.Add(new HocSinhCoNamSinh { MaHs = "HE162592", Ten = "Trần Thu Trang", Tuoi = 20, NamSinh = 2003 });
        //    danhSachHocSinh.Add(new HocSinhCoNamSinh { MaHs = "HE152868", Ten = "Hồ Việt Hải", Tuoi = 23, NamSinh = 2000 });

        //}

        //Hàm ThemDanhSachHocSinh để cho người dùng nhập vào thông tin học sinh
        public void ThemDanhSachHocSinh()
        {
            Console.WriteLine("Thêm Thông tin Học Sinh:");
            HocSinhCoNamSinh hocSinh = new HocSinhCoNamSinh();
            var dem = 0;
            Console.Write("Nhập Mã Học Sinh: ");
            string ma = null;
            bool a = true;
            while(a)
            {
                ma = KiemTraDauVao.KiemTraMa();
                int count = (from hs in danhSachHocSinh
                            where hs.MaHs.ToLower() == ma.ToLower()
                            select hs).Count();
                if(count > 0 )
                {
                    Console.WriteLine("Mã Học Sinh đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã Học Sinh: ");
                }
                else
                {
                    a= false;
                }
            }
            hocSinh.MaHs = ma;
            Console.Write("Nhập Tên Học Sinh: ");
            hocSinh.Ten = KiemTraDauVao.KiemTraTen();
            Console.Write("Nhập Năm sinh Học Sinh: ");
            int namSinh = KiemTraDauVao.KiemTraNamSinh();
            hocSinh.NamSinh = namSinh;
            hocSinh.Tuoi = (DateTime.Now.Year - namSinh);
            danhSachHocSinh.Add(hocSinh);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Học Sinh không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachHocSinh();
            }
        }

        //Hàm XuatDanhSachHocSinh để xuất ra danh sách sinh viên hiện có với không có năm sinh
        public void XuatDanhSachHocSinh()
        {
            var ketqua = from sv in danhSachHocSinh
                         select sv;
            Console.WriteLine("Danh sách Học Sinh: ");
            Console.WriteLine("Số lượng Học Sinh: " + ketqua.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-10}", "Mã Học Sinh", "Tên Học Sinh", "Tuổi");
            foreach (HocSinh sv in ketqua)
            {
                sv.inThongTin();
            }

        }

        //Hàm XuatDanhSachHocSinh để xuất ra danh sách học sinh hiện có với có thông tin năm sinh
        public void XuatDanhSachHocSinhCoNamSinh()
        {
            var ketqua = from sv in danhSachHocSinh
                         select sv;
            Console.WriteLine("Danh sách Học Sinh: ");
            Console.WriteLine("Số lượng Học Sinh: " + ketqua.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-10}", "Mã Học Sinh", "Tên Học Sinh", "Tuổi","Năm Sinh");
            foreach (HocSinhCoNamSinh sv in ketqua)
            {
                sv.inThongTin();
            }

        }

        //Hàm XoaHocSinhTheoMa cho phép người dùng xóa học sinh theo mã học sinh
        public void XoaHocSinhTheoMa()
        {
            Console.WriteLine("Xóa Thông tin Học Sinh theo Mã Học Sinh:");
            Console.Write("Nhập Mã Học Sinh: ");
            string id = KiemTraDauVao.KiemTraMa();
            var ketqua = from hs in danhSachHocSinh
                         where hs.MaHs.ToLower().Contains(id.ToLower())
                         select hs;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Thông tin Học Sinh có Mã Học Sinh = " + ketqua.SingleOrDefault().MaHs + " đã được xóa");
                danhSachHocSinh.Remove(ketqua.SingleOrDefault());
                Console.WriteLine("Danh sách Học Sinh sau khi xóa: ");
                XuatDanhSachHocSinhCoNamSinh();
            }
            else
            {
                Console.WriteLine("Không Tồn tại Học Sinh có Mã = " + id);
                Console.WriteLine("Danh sách Học Sinh không thay đổi");

            }

            Console.Write("Bạn có muốn tiếp tục xóa thông tin Học Sinh theo Mã Học Sinh không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XoaHocSinhTheoMa();
            }

        }

    }

}
