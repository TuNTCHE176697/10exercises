﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De6.QuanLyHocSinh
{
    internal class HocSinhCoNamSinh: HocSinh
    {
        public double NamSinh { get; set; }

        public HocSinhCoNamSinh() { 
        }
        public HocSinhCoNamSinh(string maHs, string ten, int tuoi, int namsinh) : base(maHs, ten, tuoi)
        {
            this.NamSinh = namsinh;
        }

        public void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15}", MaHs, Ten, Tuoi, NamSinh);
        }
    }
}
