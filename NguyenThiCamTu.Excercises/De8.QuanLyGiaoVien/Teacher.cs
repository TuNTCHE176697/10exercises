﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De8.QuanLyGiaoVien
{
    internal class Teacher
    {

        public int ID { get; set; }
        public string MaGV { get; set; }
        public string Nganh { get; set; }

        public Teacher()
        {
        }

        public Teacher(int id, string maGV, string nganh)
        {
            this.ID = id;
            this.MaGV = maGV;
            this.Nganh = nganh;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-15} {2,-15}", ID, MaGV, Nganh);
        }
    }
}
