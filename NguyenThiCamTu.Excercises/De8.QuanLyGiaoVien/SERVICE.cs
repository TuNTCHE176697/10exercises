﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De8.QuanLyGiaoVien
{
    internal class SERVICE
    {
        List<Teacher> danhSachGiaoVien = new List<Teacher>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách giáo viên
        //public void DuLieuBanDau()
        //{
        //    danhSachGiaoVien.Add(new Teacher { ID = 1, MaGV = "HE176966", Nganh = "SE" });
        //    danhSachGiaoVien.Add(new Teacher { ID = 2, MaGV = "HE238954", Nganh = "IA"});
        //    danhSachGiaoVien.Add(new Teacher { ID = 3, MaGV = "HS284543", Nganh = "UDPM" });
        //    danhSachGiaoVien.Add(new Teacher { ID = 4, MaGV = "HA184930", Nganh = "SE" });

        //}

        //Hàm ThemDanhSachGiaoVien để cho người dùng nhập vào thông tin giáo viên
        public void ThemDanhSachGiaoVien()
        {
            Console.WriteLine("Thêm Thông tin Giáo Viên:");
            Teacher giaoVien = new Teacher();
            Console.Write("Nhập ID Giáo Viên: ");
            int id = 0;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraLuaChon(1,int.MaxValue);
                int count = (from gv in danhSachGiaoVien
                             where gv.ID == id
                             select gv).Count();
                if (count > 0)
                {
                    Console.WriteLine("ID Giáo Viên đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập ID Giáo Viên: ");
                }
                else
                {
                    a = false;
                }
            }
            giaoVien.ID = id;

            Console.Write("Nhập Mã Giáo Viên: ");
            string ma = null;
            bool b = true;
            while (b)
            {
                ma = KiemTraDauVao.KiemTraTen("Mã Giáo Viên");
                int count = (from gv in danhSachGiaoVien
                             where gv.MaGV.ToLower() == ma.ToLower()
                             select gv).Count();
                if (count > 0)
                {
                    Console.WriteLine("Mã Giáo Viên đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã Giáo Viên: ");
                }
                else
                {
                    b = false;
                }
            }
            giaoVien.MaGV = ma;

            Console.Write("Nhập Ngành dạy của Giáo Viên: ");
            giaoVien.Nganh = KiemTraDauVao.KiemTraTen("Ngành dạy của Giáo Viên");

            danhSachGiaoVien.Add(giaoVien);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Giáo Viên không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachGiaoVien();
            }
        }

        //Hàm XuatDanhSachGiaoVien để xuất ra danh sách giáo viên hiện có
        public void XuatDanhSachGiaoVien()
        {
            var ketqua = from gv in danhSachGiaoVien
                         select gv;
            Console.WriteLine("Danh sách Giáo Viên: ");
            Console.WriteLine("Số lượng Giáo Viên: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-15} {2,-15}", "ID Giáo Viên", "Mã Giáo Viên", "Ngành");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }    
            

        }

        //Hàm XuatGiaoVienUDPM cho phép người dùng xuất thông tin các giáo viên ngành UDPM
        public void XuatGiaoVienUDPM()
        {
            var ketqua = from gv in danhSachGiaoVien
                         where gv.Nganh.ToLower() == "udpm"
                         select gv;
            Console.WriteLine("Danh sách Giáo Viên Ngành UDPM: ");
            Console.WriteLine("Số lượng Giáo Viên: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {

                Console.WriteLine("{0,-15} {1,-15} {2,-15}", "ID Giáo Viên", "Mã Giáo Viên", "Ngành");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }
            
        }

        //Hàm SapXepTheoNganh để xuất ra danh sách sắp xếp theo ngành dạy của giáo viên
        public void SapXepTheoNganh()
        {
            var ketqua = danhSachGiaoVien.OrderBy(x => x.Nganh);
            Console.WriteLine("Danh sách Giáo Viên sau khi sắp xếp theo Ngành: ");
            Console.WriteLine("Số lượng Giáo Viên: " + ketqua.Count());
            if (ketqua.Count() > 0)
            {

                Console.WriteLine("{0,-15} {1,-15} {2,-15}", "ID Giáo Viên", "Mã Giáo Viên", "Ngành");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }
        }
        
    }
}
