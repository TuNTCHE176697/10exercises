﻿using De1.QuanLySinhVien;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Sinh Viên: ");
    Console.WriteLine("1. Nhập Danh sách Sinh viên");
    Console.WriteLine("2. Xuất Danh sách Sinh viên");
    Console.WriteLine("3. Xuất Danh sách Sinh Viên từ 50 tuổi");
    Console.WriteLine("4. Tìm Sinh Viên theo Mã Sinh Viên");
    Console.WriteLine("5. Kế thừa");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 5);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachSinhVien();
            break;

        case 2:
            service.XuatDanhSachSinhVien();
            break;

        case 3:
            service.XuatSinhVienTren50Tuoi();
            break;

        case 4:
            service.TimSinhVienTheoMa();
            break;

        case 5:
            service.Kethua();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


