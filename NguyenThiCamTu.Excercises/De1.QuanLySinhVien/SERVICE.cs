﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De1.QuanLySinhVien
{
    internal class SERVICE
    {
        List<SinhVien> danhSachSinhVien = new List<SinhVien>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách sinh viên
        //public void DuLieuBanDau()
        //{
        //    danhSachSinhVien.Add(new SinhVien { MaSV = "HE176697", Ten = "Nguyễn Thị Cẩm Tú", NamSinh = 2003 });
        //    danhSachSinhVien.Add(new SinhVien { MaSV = "HE172659", Ten = "Trương Thị Thanh Hòa", NamSinh = 2002 });
        //    danhSachSinhVien.Add(new SinhVien { MaSV = "HE162592", Ten = "Trần Thu Trang", NamSinh = 1950 });
        //    danhSachSinhVien.Add(new SinhVien { MaSV = "HE152868", Ten = "Hồ Việt Hải", NamSinh = 1973 });

        //}

        //Hàm ThemDanhSachSinhVien để cho người dùng nhập vào thông tin sinh viên
        public void ThemDanhSachSinhVien()
        {
            Console.WriteLine("Thêm Thông tin Sinh Viên:");
            SinhVien sinhVien = new SinhVien();
            var dem = 0;
            Console.Write("Nhập Mã Sinh Viên: ");
            string ma = null;
            bool a = true;
            while(a)
            {
                ma = KiemTraDauVao.KiemTraMa();
                int count = (from sv in danhSachSinhVien
                            where sv.MaSV.ToLower() == ma.ToLower()
                            select sv).Count();
                if(count > 0 )
                {
                    Console.WriteLine("Mã Sinh Viên đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã Sinh Viên: ");
                }
                else
                {
                    a= false;
                }
            }
            sinhVien.MaSV = ma;
            Console.Write("Nhập Tên Sinh Viên: ");
            sinhVien.Ten = KiemTraDauVao.KiemTraTen();
            Console.Write("Nhập Năm sinh Sinh Viên: ");
            sinhVien.NamSinh = KiemTraDauVao.KiemTraNamSinh();
            
            danhSachSinhVien.Add(sinhVien);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Sinh Viên không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachSinhVien();
            }
        }

        //Hàm XuatDanhSachSinhVien để xuất ra danh sách sinh viên hiện có
        public void XuatDanhSachSinhVien()
        {
            var ketqua = from sv in danhSachSinhVien
                         select sv;
            Console.WriteLine("Danh sách Sinh Viên: ");
            Console.WriteLine("Số lượng Sinh Viên: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-10}", "Mã Sinh Viên", "Tên Sinh Viên", "Năm Sinh");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }    
    
        }

        //Hàm XuatSinhVienTren50Tuoi để xuất ra danh sách sinh viên từ 50 tuổi trở lên
        public void XuatSinhVienTren50Tuoi()
        {
            var ketqua = from sv in danhSachSinhVien
                         where sv.NamSinh <= (DateTime.Now.Year - 50)
                         select sv;

            Console.WriteLine("Danh sách Sinh Viên Từ 50 tuổi trở lên: ");
            Console.WriteLine("Số lượng Sinh Viên Từ 50 tuổi trở lên: " +ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-10}", "Mã Sinh Viên", "Tên Sinh Viên", "Năm Sinh");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }    
            
        }

        //Hàm TimSinhVienTheoMa cho phép người dùng tìm sinh viên theo mã sinh viên
        public void TimSinhVienTheoMa()
        {
            Console.WriteLine("Tìm Sinh Viên theo Mã Sinh Viên:");          
            Console.Write("Nhập Mã Sinh Viên: ");
            string ma = KiemTraDauVao.KiemTraMa();
            var ketqua = from sv in danhSachSinhVien
                         where sv.MaSV.ToLower().Contains(ma.ToLower())
                         select sv;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Thông tin Sinh Viên cần tìm là: ");
                Console.WriteLine("{0,-15} {1,-20} {2,-10}", "Mã Sinh Viên", "Tên Sinh Viên", "Năm Sinh");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            } else 
            {
                Console.WriteLine("Không Tồn tại Sinh Viên có Mã Sinh Viên = "+ma);
            }

            Console.Write("Bạn có muốn tiếp tục tìm kiếm thông tin Sinh Viên không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                TimSinhVienTheoMa();
            }

        }
        //Hàm Kethua cho phép người dùng khởi tạo 1 sinh viên udpm và in ra màn hình thông tin sinh viên đó
        public void Kethua()
        {
            Console.WriteLine("Thêm Thông tin Sinh Viên UDPM:");
           
            var dem = 0;
            Console.Write("Nhập Mã Sinh Viên: ");
            string ma = null;
            bool a = true;
            while (a)
            {
                ma = KiemTraDauVao.KiemTraMa();
                int count = (from sv in danhSachSinhVien
                             where sv.MaSV.ToLower() == ma.ToLower()
                             select sv).Count();
                if (count > 0)
                {
                    Console.WriteLine("Mã Sinh Viên đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã Sinh Viên: ");
                }
                else
                {
                    a = false;
                }
            }
        
            Console.Write("Nhập Tên Sinh Viên: ");
            string ten = KiemTraDauVao.KiemTraTen();
            Console.Write("Nhập Năm sinh Sinh Viên: ");
            int namsinh = KiemTraDauVao.KiemTraNamSinh();
            Console.Write("Nhập Điểm Java của Sinh Viên: ");
            double diemJV = KiemTraDauVao.KiemTraDiem(0,10);
            Console.Write("Nhập Điểm Java của Sinh Viên: ");
            double diemCS = KiemTraDauVao.KiemTraDiem(0, 10);

            SinhVienUDPM sinhVienUDPM = new SinhVienUDPM(ma, ten, namsinh, diemJV, diemCS);
            Console.WriteLine("Thông tin Sinh Viên UDPM");
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15} {4,-15}", "Mã Sinh Viên", "Tên Sinh Viên", "Năm Sinh","Điểm Java","Điểm CSharp");
            sinhVienUDPM.inThongTin();

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Sinh Viên UDPM không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                Kethua();
            }
        }
    }

}
