﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De1.QuanLySinhVien
{
    internal class SinhVien
    {
        public string MaSV { get; set; }
        public string Ten { get; set; }
        public int NamSinh { get; set; }

        public SinhVien () { 
        }

        public SinhVien(string maSV, string ten, int namSinh)
        {
            this.MaSV = maSV;
            this.Ten = ten;
            this.NamSinh = namSinh;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10}", MaSV, Ten, NamSinh);
        }
    }

}
