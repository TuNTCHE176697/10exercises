﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De1.QuanLySinhVien
{
    internal class SinhVienUDPM : SinhVien
    {
        public double DiemJava { get; set; }
        public double DiemCsharp { get; set; }

        public SinhVienUDPM() { 
        }
        public SinhVienUDPM(string maSV, string ten, int namSinh, double diemJava, double diemCsharp) : base(maSV, ten, namSinh)
        {
            this.DiemJava = diemJava;
            this.DiemCsharp = diemCsharp;
        }

        public override void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15} {4,-15}", MaSV, Ten, NamSinh, DiemJava, DiemCsharp);

        }
    }
}
