﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De2.QuanLyGiaoVien
{
    internal class GiaoVien
    {

        public int ID { get; set; }
        public string Ten { get; set; }
        public double SoGioDay { get; set; }

        public GiaoVien()
        {
        }

        public GiaoVien(int id, string ten, double soGioDay)
        {
            this.ID = id;
            this.Ten = ten;
            this.SoGioDay = soGioDay;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10}", ID, Ten, SoGioDay);
        }
    }
}
