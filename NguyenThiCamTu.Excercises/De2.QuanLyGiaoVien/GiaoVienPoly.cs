﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De2.QuanLyGiaoVien
{
    internal class GiaoVienPoly : GiaoVien
    {
        public string NganhDay { get; set; }

        public GiaoVienPoly() { }

        public GiaoVienPoly(int id,  string ten, double gioday, string nganh):base(id,ten, gioday)
        {
            this.NganhDay = nganh;
        }

        public override void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15}", ID, Ten, SoGioDay, NganhDay);

        }
    }
}
