﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De2.QuanLyGiaoVien
{
    internal class SERVICE
    {
        List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách giáo viên
        //public void DuLieuBanDau()
        //{
        //    danhSachGiaoVien.Add(new GiaoVien { ID = 1, Ten = "Nguyễn Thị Cẩm Tú", SoGioDay = 140 });
        //    danhSachGiaoVien.Add(new GiaoVien { ID = 2, Ten = "Trương Thị Thanh Hòa", SoGioDay = 345.8 });
        //    danhSachGiaoVien.Add(new GiaoVien { ID = 3, Ten = "Trần Thu Trang", SoGioDay = 232 });
        //    danhSachGiaoVien.Add(new GiaoVien { ID = 4, Ten = "Hồ Việt Hải", SoGioDay = 466.5 });

        //}

        //Hàm ThemDanhSachGiaoVien để cho người dùng nhập vào thông tin giáo viên
        public void ThemDanhSachGiaoVien()
        {
            Console.WriteLine("Thêm Thông tin Giáo Viên:");
            GiaoVien giaoVien = new GiaoVien();
            var dem = 0;
            Console.Write("Nhập ID Giáo Viên: ");
            int id = 0;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraLuaChon(1,int.MaxValue);
                int count = (from gv in danhSachGiaoVien
                             where gv.ID == id
                             select gv).Count();
                if (count > 0)
                {
                    Console.WriteLine("ID Giáo Viên đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập ID Giáo Viên: ");
                }
                else
                {
                    a = false;
                }
            }
            giaoVien.ID = id;
            Console.Write("Nhập Tên Giáo Viên: ");
            giaoVien.Ten = KiemTraDauVao.KiemTraTen();
            Console.Write("Nhập Số Giờ Dạy của Giáo Viên: ");
            giaoVien.SoGioDay = KiemTraDauVao.KiemTraGioDay(0,double.MaxValue);

            danhSachGiaoVien.Add(giaoVien);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Giáo Viên không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachGiaoVien();
            }
        }

        //Hàm XuatDanhSachGiaoVien để xuất ra danh sách giáo viên hiện có
        public void XuatDanhSachGiaoVien()
        {
            var ketqua = from gv in danhSachGiaoVien
                         select gv;
            Console.WriteLine("Danh sách Giáo Viên: ");
            Console.WriteLine("Số lượng Giáo Viên: " + ketqua.Count());
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-10}", "ID Giáo Viên", "Tên Giáo Viên", "Số giờ dạy");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }    
            

        }

        //Hàm XuatGiaoVienTheoKhoangGioDay để xuất ra danh sách sinh viên từ 50 tuổi trở lên
        public void XuatGiaoVienTheoKhoangGioDay()
        {
            Console.WriteLine("Xuất Danh sách Giáo Viên với Số giờ dạy theo yêu cầu:");
            Console.Write("Nhập Số giờ dạy tối thiểu: ");
            double minGio = KiemTraDauVao.KiemTraGioDay(0, double.MaxValue);
            
            Console.Write("Nhập Số giờ dạy tối đa: ");
            double maxGio = 0;
            bool a = true;
            while (a)
            {
                maxGio = KiemTraDauVao.KiemTraGioDay(0, double.MaxValue);
                
                if (minGio > maxGio)
                {
                    Console.WriteLine("Số giờ tối đa phải lớn hơn số giờ tối thiểu! Xin hãy nhập lại!");
                    Console.Write("Nhập Số giờ dạy tối đa: ");
                }
                else
                {
                    a = false;
                }
            }

            var ketqua = from gv in danhSachGiaoVien
                         where gv.SoGioDay >= minGio && gv.SoGioDay <= maxGio
                         select gv;
            Console.WriteLine("Số lượng Giáo Viên có Số giờ dạy trong khoảng " + minGio + " giờ đến " + maxGio + " giờ: " + ketqua.Count());
            
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Danh sách Giáo Viên có Số giờ dạy trong khoảng " + minGio + " giờ đến " + maxGio + " giờ: ");
                Console.WriteLine("{0,-15} {1,-20} {2,-10}", "ID Giáo Viên", "Tên Giáo Viên", "Số giờ dạy");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }

            Console.Write("Bạn có muốn tiếp tục tìm thông tin Giáo Viên Theo khoảng giờ dạy không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XuatGiaoVienTheoKhoangGioDay();
            }

        }

        //Hàm XoaGiaoVienTheoID cho phép người dùng xóa giáo viên theo id giáo viên
        public void XoaGiaoVienTheoID()
        {
            Console.WriteLine("Xóa Thông tin Giáo Viên theo ID Giáo Viên:");
            Console.Write("Nhập ID Giáo Viên: ");
            int id = KiemTraDauVao.KiemTraLuaChon(0, int.MaxValue);
            var ketqua = from sv in danhSachGiaoVien
                         where sv.ID == id
                         select sv;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Thông tin Giáo Viên có ID = "+id+" đã được xóa");
                danhSachGiaoVien.Remove(ketqua.SingleOrDefault());
                Console.WriteLine("Danh sách Giáo Viên sau khi xóa: ");
                XuatDanhSachGiaoVien();
            }
            else
            {
                Console.WriteLine("Không Tồn tại Giáo Viên có ID = " + id);
                Console.WriteLine("Danh sách Giáo viên không thay đổi");

            }

            Console.Write("Bạn có muốn tiếp tục xóa thông tin Giao Viên không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XoaGiaoVienTheoID();
            }

        }
        //Hàm Kethua cho phép người dùng khởi tạo 1 Giáo viên Poly và in ra màn hình thông tin giáo viên đó
        public void Kethua()
        {
            Console.WriteLine("Thêm Thông tin Giáo Viên Poly:");

            GiaoVienPoly giaoVien = new GiaoVienPoly();
            var dem = 0;
            Console.Write("Nhập ID Giáo Viên: ");
            int id = 0;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraLuaChon(1, int.MaxValue);
                int count = (from gv in danhSachGiaoVien
                             where gv.ID == id
                             select gv).Count();
                if (count > 0)
                {
                    Console.WriteLine("Mã Giáo Viên đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã Giáo Viên: ");
                }
                else
                {
                    a = false;
                }
            }
            giaoVien.ID = id;
            Console.Write("Nhập Tên Giáo Viên: ");
            giaoVien.Ten = KiemTraDauVao.KiemTraTen();
            Console.Write("Nhập Số Giờ Dạy của Giáo Viên: ");
            giaoVien.SoGioDay = KiemTraDauVao.KiemTraGioDay(0, double.MaxValue);
            Console.Write("Nhập Ngành dạy của Giáo Viên: ");
            giaoVien.NganhDay = KiemTraDauVao.KiemTraNganh();

            Console.WriteLine("Thông tin Giáo Viên Poly");
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15} ", "ID Giáo Viên", "Tên Giáo Viên", "Số Giờ Dạy", "Ngành dạy");
            giaoVien.inThongTin();

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Giáo Viên Poly không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                Kethua();
            }
        }
    }
}
