﻿using De2.QuanLyGiaoVien;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Giáo Viên: ");
    Console.WriteLine("1. Nhập Danh sách Giáo viên");
    Console.WriteLine("2. Xuất Danh sách Giáo viên");
    Console.WriteLine("3. Xuất Danh sách Giáo Viên có số giờ dạy theo yêu cầu");
    Console.WriteLine("4. Xóa Giáo Viên theo ID Giáo Viên");
    Console.WriteLine("5. Kế thừa");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 5);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachGiaoVien();
            break;

        case 2:
            service.XuatDanhSachGiaoVien();
            break;

        case 3:
            service.XuatGiaoVienTheoKhoangGioDay();
            break;

        case 4:
            service.XoaGiaoVienTheoID();
            break;

        case 5:
            service.Kethua();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


