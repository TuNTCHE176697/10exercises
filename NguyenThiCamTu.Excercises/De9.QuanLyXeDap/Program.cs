﻿using De9.QuanLyXeDap;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Xe Máy: ");
    Console.WriteLine("1. Nhập Danh sách Xe Máy");
    Console.WriteLine("2. Xuất Danh sách Xe Máy");
    Console.WriteLine("3. Xuất Danh sách Xe Máy của Hãng HONDA");
    Console.WriteLine("4. Sắp xếp Xe máy theo ID giảm dần");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 4);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachXeMay();
            break;

        case 2:
            service.XuatDanhSachXeMay();
            break;

        case 3:
            service.XuatXeMayHONDA();
            break;

        case 4:
            service.SapXepTheoID();
            break;
        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


