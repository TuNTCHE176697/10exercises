﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De9.QuanLyXeDap
{
    internal class Bike
    {

        public int ID { get; set; }
        public string Ten { get; set; }
        public string HSX { get; set; }

        public Bike()
        {
        }

        public Bike(int id, string ten, string hsx)
        {
            this.ID = id;
            this.Ten = ten;
            this.HSX = hsx;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-15}", ID, Ten, HSX);
        }
    }
}
