﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De9.QuanLyXeDap
{
    internal class SERVICE
    {
        List<Bike> danhSachPhuongTien = new List<Bike>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách xe máy
        //public void DuLieuBanDau()
        //{
        //    danhSachPhuongTien.Add(new Bike { ID = 1, Ten = "Africa Twin 2023", HSX = "HONDA" });
        //    danhSachPhuongTien.Add(new Bike { ID = 2, Ten = "Wave Alpha 110cc", HSX = "HONDA"});
        //    danhSachPhuongTien.Add(new Bike { ID = 3, Ten = "Suzuki Raider R150", HSX = "SUZUKI" });
        //    danhSachPhuongTien.Add(new Bike { ID = 4, Ten = "Satria F150", HSX = "SUZUKI" });

        //}

        //Hàm ThemDanhSachXeMay để cho người dùng nhập vào thông tin xe máy
        public void ThemDanhSachXeMay()
        {
            Console.WriteLine("Thêm Thông tin Xe Máy:");
            Bike xeMay = new Bike();
            Console.Write("Nhập ID Xe máy: ");
            int id = 0;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraLuaChon(1,int.MaxValue);
                int count = (from xm in danhSachPhuongTien
                             where xm.ID == id
                             select xm).Count();
                if (count > 0)
                {
                    Console.WriteLine("ID Xe Máy đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập ID Xe Máy: ");
                }
                else
                {
                    a = false;
                }
            }
            xeMay.ID = id;

            Console.Write("Nhập Tên Xe Máy: ");      
            xeMay.Ten = KiemTraDauVao.KiemTraTen("Tên Xe Máy");

            Console.Write("Nhập Hãng Sản Xuất của Xe Máy: ");
            xeMay.HSX = KiemTraDauVao.KiemTraTen("Hãng Sản Xuất của Xe Máy");

            danhSachPhuongTien.Add(xeMay);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Xe Máy không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachXeMay();
            }
        }

        //Hàm XuatDanhSachXeMay để xuất ra danh sách xe máy hiện có
        public void XuatDanhSachXeMay()
        {
            var ketqua = from xm in danhSachPhuongTien
                         select xm;
            Console.WriteLine("Danh sách Xe Máy: ");
            Console.WriteLine("Số lượng Xe Máy: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-20} {2,-15}", "ID Xe Máy", "Tên Xe Máy", "Hãng Sản Xuất");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }    
            

        }

        //Hàm XuatXeMayHONDA cho phép người dùng xuất thông tin các xe máy ngành HONDA
        public void XuatXeMayHONDA()
        {
            var ketqua = from xm in danhSachPhuongTien
                         where xm.HSX.ToLower() == "honda"
                         select xm;
            Console.WriteLine("Danh sách Xe Máy Hãng HONDA: ");
            Console.WriteLine("Số lượng Xe Máy: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {

                Console.WriteLine("{0,-15} {1,-20} {2,-15}", "ID Xe Máy", "Tên Xe Máy", "Hãng sản xuất");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }
            
        }

        //Hàm SapXepTheoID xuất danh sách xe máy có id theo thứ tự giảm dần
        public void SapXepTheoID()
        {
            var ketqua = danhSachPhuongTien.OrderByDescending(x => x.ID);
            Console.WriteLine("Danh sách Xe Máy sau khi sắp xếp giảm dần theo ID: ");
            Console.WriteLine("Số lượng Xe Máy: " + ketqua.Count());
            if (ketqua.Count() > 0)
            {

                Console.WriteLine("{0,-15} {1,-20} {2,-15}", "ID Xe Máy", "Tên Xe Máy", "Hãng Sản Xuất");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }
        }
        
    }
}
