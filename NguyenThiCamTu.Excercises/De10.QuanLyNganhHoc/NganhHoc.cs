﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De10.QuanLyNganhHoc
{
    internal class NganhHoc
    {

        public int ID { get; set; }
        public string Ten { get; set; }
        public int? SoKyHoc { get; set; }

        public NganhHoc()
        {
        }

        public NganhHoc(int id, string ten, int? soKyHoc)
        {
            this.ID = id;
            this.Ten = ten;
            this.SoKyHoc = soKyHoc;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-30} {2,-10}", ID, Ten, SoKyHoc);
        }
    }
}
