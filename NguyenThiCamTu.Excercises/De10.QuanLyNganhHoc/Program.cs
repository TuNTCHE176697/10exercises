﻿using De10.QuanLyNganhHoc;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Ngành Học: ");
    Console.WriteLine("1. Nhập Danh sách Ngành Học");
    Console.WriteLine("2. Xuất Danh sách Ngành Học");
    Console.WriteLine("3. Xuất Danh sách Ngành Học có Số Kỳ Học lớn hơn 6");
    Console.WriteLine("4. Xóa Kỳ học theo ID Ngành Học");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 4);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachNganhHoc();
            break;

        case 2:
            service.XuatDanhSachNganhHoc();
            break;

        case 3:
            service.XuatNganhHocHon6Ky();
            break;

        case 4:
            service.XoaKyHocTheoID();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


