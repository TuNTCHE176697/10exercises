﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De10.QuanLyNganhHoc
{
    internal class SERVICE
    {
        List<NganhHoc> danhSachNganhHoc = new List<NganhHoc>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách ngành học
        //public void DuLieuBanDau()
        //{
        //    danhSachNganhHoc.Add(new NganhHoc { ID = 1, Ten = "Kỹ Thuật Phần Mềm", SoKyHoc = 6 });
        //    danhSachNganhHoc.Add(new NganhHoc { ID = 2, Ten = "An Toàn Thông Tin", SoKyHoc = 7 });
        //    danhSachNganhHoc.Add(new NganhHoc { ID = 3, Ten = "Trí Tuệ Nhân Tạo", SoKyHoc = 6 });
        //    danhSachNganhHoc.Add(new NganhHoc { ID = 4, Ten = "Quản Trị Kinh Doanh", SoKyHoc = 9 });

        //}

        //Hàm ThemDanhSachNganhHoc để cho người dùng nhập vào thông tin ngành học
        public void ThemDanhSachNganhHoc()
        {
            Console.WriteLine("Thêm Thông tin Ngành Học:");
            NganhHoc nganhHoc = new NganhHoc();
            var dem = 0;
            Console.Write("Nhập ID Ngành Học: ");
            int id = 0;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraLuaChon(1,int.MaxValue);
                int count = (from nh in danhSachNganhHoc
                             where nh.ID == id
                             select nh).Count();
                if (count > 0)
                {
                    Console.WriteLine("ID Ngành học đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập ID Ngành học: ");
                }
                else
                {
                    a = false;
                }
            }
            nganhHoc.ID = id;

            Console.Write("Nhập Tên Ngành học: ");
            string ten = null;
            bool b = true;
            while (b)
            {
                ten = KiemTraDauVao.KiemTraNganh();
                int count = (from nh in danhSachNganhHoc
                             where nh.Ten.ToLower() == ten.ToLower()
                             select nh).Count();
                if (count > 0)
                {
                    Console.WriteLine("Tên Ngành học đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Tên Ngành học: ");
                }
                else
                {
                    b = false;
                }
            }
            nganhHoc.Ten = ten;
            
            Console.Write("Nhập Số Kỳ học của Ngành học: ");
            nganhHoc.SoKyHoc = KiemTraDauVao.KiemTraLuaChon(0,int.MaxValue);

            danhSachNganhHoc.Add(nganhHoc);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Ngành học không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachNganhHoc();
            }
        }

        //Hàm XuatDanhSachNganhHoc để xuất ra danh sách ngành học hiện có
        public void XuatDanhSachNganhHoc()
        {
            var ketqua = from nh in danhSachNganhHoc
                         select nh;
            Console.WriteLine("Danh sách Ngành học: ");
            Console.WriteLine("Số lượng Ngành học: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-30} {2,-10}", "ID Ngành học", "Tên Ngành học", "Số Kỳ học");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }    
            
        }

        //Hàm XuatNganhHocHon6Ky để xuất ra danh sách ngành học có số kỳ học lớn hơn 6
        public void XuatNganhHocHon6Ky()
        {
            var ketqua = from nh in danhSachNganhHoc
                         where nh.SoKyHoc > 6
                         select nh;
            Console.WriteLine("Danh sách Ngành học có Số Kỳ học lớn hơn 6: ");
            Console.WriteLine("Số lượng Ngành học có Số Kỳ học lớn hơn 6: " + ketqua.Count());
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-30} {2,-10}", "ID Ngành học", "Tên Ngành học", "Số Kỳ học");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }

        }

        //Hàm XoaKyHocTheoID cho phép người dùng xóa kỳ học của ngành học theo ID
        public void XoaKyHocTheoID()
        {
            Console.WriteLine("Xóa Kỳ học của Ngành học theo ID Ngành học:");
            Console.Write("Nhập ID Ngành học: ");
            int id = KiemTraDauVao.KiemTraLuaChon(0, int.MaxValue);
            var ketqua = from nh in danhSachNganhHoc
                         where nh.ID == id
                         select nh;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Kỳ học của Ngành học có ID = "+id+" đã được xóa");
                ketqua.SingleOrDefault().SoKyHoc = null;
                Console.WriteLine("Danh sách Ngành học sau khi thực hiện xóa: ");
                XuatDanhSachNganhHoc();
            }
            else
            {
                Console.WriteLine("Không Tồn tại Ngành học có ID = " + id);
                Console.WriteLine("Danh sách Ngành học không thay đổi");

            }

            Console.Write("Bạn có muốn tiếp tục xóa Kỳ học của Ngành học theo ID Ngành học không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XoaKyHocTheoID();
            }

        
        }
    }
}
