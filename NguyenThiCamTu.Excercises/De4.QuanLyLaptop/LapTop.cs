﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De4.QuanLyLapTop
{
    internal class LapTop
    {

        public int ID { get; set; }
        public string MaLapTop { get; set; }
        public double KichThuocMH { get; set; }

        public LapTop()
        {
        }

        public LapTop(int id, string maLaptop, double kichThuocMH)
        {
            this.ID = id;
            this.MaLapTop = maLaptop;
            this.KichThuocMH = kichThuocMH;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-25} {2,-20}", ID, MaLapTop, KichThuocMH);
        }
    }
}
