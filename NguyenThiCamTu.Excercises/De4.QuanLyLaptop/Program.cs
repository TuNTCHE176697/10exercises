﻿using De4.QuanLyLapTop;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý LapTop: ");
    Console.WriteLine("1. Nhập Danh sách LapTop");
    Console.WriteLine("2. Xuất Danh sách LapTop");
    Console.WriteLine("3. Xóa LapTop theo Mã LapTop");
    Console.WriteLine("4. Xuất Kích Thước Màn Hình trong khoảng yêu cầu");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 4);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachLapTop();
            break;

        case 2:
            service.XuatDanhSachLapTop();
            break;

        case 3:
            service.XoaLapTopTheoMa();
            break;

        case 4:
            service.XuatKichThuocTheoYeuCau();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


