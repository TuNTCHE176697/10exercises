﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De4.QuanLyLapTop
{
    internal class SERVICE
    {
        List<LapTop> danhSachLapTop = new List<LapTop>();

        //Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách Laptop
        public void DuLieuBanDau()
        {
            danhSachLapTop.Add(new LapTop { ID = 1, MaLapTop = "ASUS 1", KichThuocMH = 2220});
            danhSachLapTop.Add(new LapTop { ID = 2, MaLapTop = "ASUS 2", KichThuocMH = 1000.5});
            danhSachLapTop.Add(new LapTop { ID = 3, MaLapTop = "LENOVO 1", KichThuocMH = 2300.5 });
            danhSachLapTop = danhSachLapTop.OrderBy(x=>x.KichThuocMH).ToList();
        }
        
        //Hàm ThemDanhSachLapTop để cho người dùng nhập vào thông tin Laptop
        public void ThemDanhSachLapTop()
        {
            Console.WriteLine("Thêm Thông tin LapTop:");
            LapTop lapTop = new LapTop();
            Console.Write("Nhập ID LapTop: ");
            int id = 0;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraLuaChon(1,int.MaxValue);
                int count = (from lt in danhSachLapTop
                             where lt.ID == id
                             select lt).Count();
                if (count > 0)
                {
                    Console.WriteLine("ID LapTop đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập ID LapTop: ");
                }
                else
                {
                    a = false;
                }
            }
            lapTop.ID = id;

            Console.Write("Nhập Mã LapTop: ");
            string ma = null;
            bool b = true;
            while (b)
            {
                ma = KiemTraDauVao.KiemTraMa();
                int count = (from lt in danhSachLapTop
                             where lt.MaLapTop.ToLower() == ma.ToLower()
                             select lt).Count();
                if (count > 0)
                {
                    Console.WriteLine("Mã LapTop đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã LapTop: ");
                }
                else
                {
                    b = false;
                }
            }
            lapTop.MaLapTop = ma;
          
            Console.Write("Nhập Kích thước màn hình của LapTop: ");
            lapTop.KichThuocMH = KiemTraDauVao.KiemTraKichThuoc(0,double.MaxValue);

            danhSachLapTop.Add(lapTop);
            danhSachLapTop = danhSachLapTop.OrderBy(x => x.KichThuocMH).ToList();

            Console.Write("Bạn có muốn tiếp tục nhập thông tin LapTop không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachLapTop();
            }
        }

        //Hàm XuatDanhSachLapTop để xuất ra danh sách laptop hiện có
        public void XuatDanhSachLapTop()
        {
            var ketqua = from lt in danhSachLapTop
                         select lt;
            Console.WriteLine("Danh sách LapTop: ");
            Console.WriteLine("Số lượng LapTop: " + ketqua.Count());
            Console.WriteLine("{0,-15} {1,-25} {2,-20}", "ID LapTop", "Mã LapTop", "Kích thước màn hình");
            foreach (var sv in ketqua)
            {
                sv.inThongTin();
            }

        }

        //Hàm XuatKichThuocTheoYeuCau để xuất ra danh sách kích thước màn hình trong khoảng người dùng nhập vào
        public void XuatKichThuocTheoYeuCau()
        {
            Console.WriteLine("Xuất Danh sách Kích Thước Màn Hình theo yêu cầu:");
            Console.Write("Nhập Kích Thước Màn Hình tối thiểu: ");
            double minGio = KiemTraDauVao.KiemTraKichThuoc(0, double.MaxValue);
            
            Console.Write("Nhập Kích Thước Màn Hình tối đa: ");
            double maxGio = 0;
            bool a = true;
            while (a)
            {
                maxGio = KiemTraDauVao.KiemTraKichThuoc(0, double.MaxValue);
                
                if (minGio > maxGio)
                {
                    Console.WriteLine("Kích Thước Màn Hình tối đa phải lớn hơn Kích Thước Màn Hình tối thiểu! Xin hãy nhập lại!");
                    Console.Write("Nhập Kích Thước Màn Hình tối đa: ");
                }
                else
                {
                    a = false;
                }
            }

            var ketqua = from lt in danhSachLapTop
                         where lt.KichThuocMH >= minGio && lt.KichThuocMH <= maxGio
                         select lt;
            Console.WriteLine("Số lượng Kích Thước Màn Hình có Kích Thước trong khoảng " + minGio + " đến " + maxGio + ": " + ketqua.Count());
            
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Danh sách Kích Thước Màn Hình có Kích Thước trong khoảng " + minGio + " đến " + maxGio + ": ");
                Console.WriteLine("{0,-20}","Kích Thước Màn Hình");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }

            Console.Write("Bạn có muốn tiếp tục tìm thông tin Kích Thước Màn Hình trong khoảng yêu cầu không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XuatKichThuocTheoYeuCau();
            }

        }

        //Hàm XoaLapTopTheoMa cho phép người dùng xóa laptop theo mã laptop
        public void XoaLapTopTheoMa()
        {
            Console.WriteLine("Xóa Thông tin LapTop theo Mã LapTop:");
            Console.Write("Nhập Mã LapTop: ");
            string id = KiemTraDauVao.KiemTraMa();
            var ketqua = from lt in danhSachLapTop
                         where lt.MaLapTop.ToLower() == id.ToLower()
                         select lt;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Thông tin LapTop có Mã = "+ketqua.SingleOrDefault().MaLapTop+" đã được xóa");
                danhSachLapTop.Remove(ketqua.SingleOrDefault());
                Console.WriteLine("Danh sách LapTop sau khi xóa: ");
                XuatDanhSachLapTop();
            }
            else
            {
                Console.WriteLine("Không Tồn tại LapTop có Mã = " + id);
                Console.WriteLine("Danh sách LapTop không thay đổi");

            }

            Console.Write("Bạn có muốn tiếp tục xóa thông tin LapTop theo Mã LapTop không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XoaLapTopTheoMa();
            }

        }
        
    }
}
