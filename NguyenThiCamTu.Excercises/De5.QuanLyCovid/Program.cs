﻿using De5.QuanLyCovid;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Virus Covid: ");
    Console.WriteLine("1. Nhập Danh sách Virus Covid");
    Console.WriteLine("2. Xuất Danh sách Virus Covid");
    Console.WriteLine("3. Xuất Danh sách Virus Covid có mã bắt đầu bằng CO");
    Console.WriteLine("4. Xuất Danh sách Virus Covis có Năm phát hiện sắp xếp thứ tự tăng dần");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 4);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachVirus();
            break;

        case 2:
            service.XuatDanhSachCovid();
            break;

        case 3:
            service.XuatDanhSachCovidCO();
            break;

        case 4:
            service.SapXepTheoNamPhatHien();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


