﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De5.QuanLyCovid
{
    internal class Covid
    {
        public string MaCovid { get; set; }
        public string Ten {  get; set; }
        public int NamPhatHien { get; set; }
        public Covid(){
        }
        public Covid(string maCovid, string ten, int namPhatHien)
        {
            this.MaCovid = maCovid;
            this.Ten = ten;
            this.NamPhatHien = namPhatHien;
        }

        public void inThongTin()
        {
            Console.WriteLine("{0,-12} {1,-20} {2,-15}",MaCovid,Ten,NamPhatHien);
        }
    }
}
