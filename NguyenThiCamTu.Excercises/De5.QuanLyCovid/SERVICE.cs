﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De5.QuanLyCovid
{
    internal class SERVICE
    {
        List<Covid> danhSachCovid = new List<Covid>();

        ////Hàm DuLieuBanDau dùng để khởi tạo dữ liệu ban đầu cho danh sách Covid
        //public void DuLieuBanDau()
        //{
        //    danhSachCovid.Add(new Covid { MaCovid = "HCoV-229E", Ten = "Coronavirus 229E", NamPhatHien = 2003 });
        //    danhSachCovid.Add(new Covid { MaCovid = "SARS-CoV-2", Ten = "Coronavirus Vũ Hán", NamPhatHien = 2019 });
        //    danhSachCovid.Add(new Covid { MaCovid = "COV-OC43", Ten = "Coronavirus OC43", NamPhatHien = 2004 });
        //}

        //Hàm ThemDanhSachVirus để cho người dùng nhập vào thông tin Virus
        public void ThemDanhSachVirus()
        {
            Console.WriteLine("Thêm Thông tin Virus Covid:");
            Covid virus = new Covid();
            Console.Write("Nhập Mã Virus Covid: ");
            string ma = null;
            bool a = true;
            while (a)
            {
                ma = KiemTraDauVao.KiemTraMa();
                int count = (from cv in danhSachCovid
                             where cv.MaCovid.ToLower() == ma.ToLower()
                             select cv).Count();
                if (count > 0)
                {
                    Console.WriteLine("Mã Covid đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Mã Covid: ");
                }
                else
                {
                    a = false;
                }
            }
            virus.MaCovid = ma;

            Console.Write("Nhập Tên Covid: ");
            string ten = null;
            bool b = true;
            while (b)
            {
                ten = KiemTraDauVao.KiemTraMa();
                int count = (from cv in danhSachCovid
                             where cv.Ten.ToLower() == ten.ToLower()
                             select cv).Count();
                if (count > 0)
                {
                    Console.WriteLine("Tên Covid đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Tên Covid: ");
                }
                else
                {
                    b = false;
                }
            }
            virus.Ten = ten;

            Console.Write("Nhập Năm Phát Hiện Virus Covid: ");
            virus.NamPhatHien = KiemTraDauVao.KiemTraNam();

            danhSachCovid.Add(virus);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Virus Covid không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachVirus();
            }
        }

        //Hàm XuatDanhSachCovid để xuất ra danh sách Virus Covid hiện có
        public void XuatDanhSachCovid()
        {
            var ketqua = from vr in danhSachCovid
                         select vr;
            Console.WriteLine("Danh sách Virus Covid: ");
            Console.WriteLine("Số lượng Virus Covid: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-12} {1,-20} {2,-15}", "Mã Covid", "Tên Covid", "Năm Phát Hiện");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }
            

        }

        //Hàm XuatDanhSachCovidCO để xuất ra danh sách virus bắt đầu với "CO"
        public void XuatDanhSachCovidCO()
        {
            Console.WriteLine("Xuất Danh sách Virus Covid bắt đầu với CO:");

            var ketqua = from cv in danhSachCovid
                         where cv.MaCovid.StartsWith("CO")
                         select cv;
            Console.WriteLine("Số lượng Virus Covid bắt đầu với CO: " + ketqua.Count());

            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Danh sáchVirus Covid bắt đầu với CO: ");
                Console.WriteLine("{0,-12} {1,-20} {2,-15}", "Mã Covid", "Tên Covid", "Năm Phát Hiện");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }

        }

        //Hàm SapXepTheoNamPhatHien xuất danh sách virus covid sắp xếp năm phát hiện theo thứ tự tăng dần
        public void SapXepTheoNamPhatHien()
        {
            Console.WriteLine("Xuất Danh sách Virus Covid sắp xếp Năm Phát Hiện theo thứ tự tăng dần:");
            var ketqua = danhSachCovid.OrderBy(x => x.NamPhatHien);
            Console.WriteLine("{0,-12} {1,-20} {2,-15}", "Mã Covid", "Tên Covid", "Năm Phát Hiện");
            foreach (var sv in ketqua)
            {
                sv.inThongTin();
            }
        }
    }
}
