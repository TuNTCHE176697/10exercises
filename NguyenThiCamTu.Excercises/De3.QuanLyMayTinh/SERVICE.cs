﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De3.QuanLyMayTinh
{
    internal class SERVICE
    {
        List<MayTinh> danhSachMayTinh = new List<MayTinh>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách máy tính
        //public void DuLieuBanDau()
        //{
        //    danhSachMayTinh.Add(new MayTinh { ID = "MT1", Ten = "Asus ROG Strix G16", TrongLuong = 1 });
        //    danhSachMayTinh.Add(new MayTinh { ID = "MT2", Ten = "Asus Vivobook 15 OLED", TrongLuong = 1.4f });
        //    danhSachMayTinh.Add(new MayTinh { ID = "MT3", Ten = "Lenovo Legion 5", TrongLuong = 2.5f });
        //    danhSachMayTinh.Add(new MayTinh { ID = "MT4", Ten = "LG Gram 14 2022", TrongLuong = 2});
        //}
        //Hàm TuTrienKhaiID dùng để tự triển khai ID máy tính lúc thêm mới máy tính
        public string TuTrienKhaiID()
        {
            int tongMayTinh = danhSachMayTinh.Count;
            string idMayTinh = "MT1";

            if (tongMayTinh > 0)
            {
                idMayTinh = "MT"+ (danhSachMayTinh.Count + 1);
            }

            return idMayTinh;
        }


        //Hàm ThemDanhSachMayTinh để cho người dùng nhập vào thông tin máy tính
        public void ThemDanhSachMayTinh()
        {

            Console.WriteLine("Thêm Thông tin Máy Tính:");
            MayTinh mayTinh = new MayTinh();
            mayTinh.ID = TuTrienKhaiID();
            Console.Write("Nhập Tên Máy Tính: ");
            string id = null;
            bool a = true;
            while (a)
            {
                id = KiemTraDauVao.KiemTraTen();
                int count = (from mt in danhSachMayTinh
                             where mt.Ten.ToLower() == id.ToLower()
                             select mt).Count();
                if (count > 0)
                {
                    Console.WriteLine("Tên Máy Tính đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập Tên Máy Tính: ");
                }
                else
                {
                    a = false;
                }
            }
            mayTinh.Ten = id;
            Console.Write("Nhập Trọng Lượng của Máy Tính: ");
            mayTinh.TrongLuong = KiemTraDauVao.KiemTraTrongLuong(0,float.MaxValue); 

            danhSachMayTinh.Add(mayTinh);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Máy Tính theo ID không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachMayTinh();
            }
        }

        //Hàm XuatDanhSachMayTinh để xuất ra danh sách máy tính hiện có
        public void XuatDanhSachMayTinh()
        {
            var ketqua = from mt in danhSachMayTinh
                         select mt;
            Console.WriteLine("Danh sách Máy Tính: ");
            Console.WriteLine("Số lượng Máy Tính: " + ketqua.Count());
            if(ketqua.Count() > 0)
            {
                Console.WriteLine("{0,-15} {1,-35} {2,-12}", "ID Máy Tính", "Tên Máy Tính", "Trọng lượng");
                foreach (var sv in ketqua)
                {
                    sv.inThongTin();
                }
            }
           

        }

        //Hàm XuatTrongLuongTheoTen để xuất ra trọng lượng theo tên máy tính
        public void XuatTrongLuongTheoTen()
        {
            Console.WriteLine("Xuất Trọng Lượng Máy Tính theo Tên Máy Tính:");
            Console.Write("Nhập Tên Máy tính: ");
            string ten = KiemTraDauVao.KiemTraTen();
        
            var ketqua = from mt in danhSachMayTinh
                         where mt.Ten.ToLower().Contains(ten.ToLower())
                         select mt;
            Console.WriteLine("Số lượng Máy Tính có tên gần đúng yêu cầu: " + ketqua.Count());

            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Danh sách Trọng Lượng Máy Tính có tên gần đúng với "+ten+" : ");
                Console.WriteLine("{0,-35} {1,-10}", "Tên Máy Tính", "Trọng lượng");
                foreach (var sv in ketqua)
                {
                    Console.WriteLine("{0,-35} {1,-10}", sv.Ten, sv.TrongLuong);

                }
            }

            Console.Write("Bạn có muốn tiếp tục tìm trọng lượng máy tính theo tên máy tính không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XuatTrongLuongTheoTen();
            }

        }

        //Hàm XoaMayTinhTheoID cho phép người dùng xóa máy tính theo id máy tính
        public void XoaMayTinhTheoID()
        {
            Console.WriteLine("Xóa Thông tin Máy Tính theo ID Máy Tính:");
            Console.Write("Nhập ID Máy Tính: ");
            string id = KiemTraDauVao.KiemTraTen();
            var ketqua = from mt in danhSachMayTinh
                         where mt.ID.ToLower() == id.ToLower()
                         select mt;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Thông tin Máy Tính có ID = " + ketqua.SingleOrDefault().ID + " đã được xóa");
                danhSachMayTinh.Remove(ketqua.SingleOrDefault());
                Console.WriteLine("Danh sách Giáo Viên sau khi xóa: ");
                XuatDanhSachMayTinh();
            }
            else
            {
                Console.WriteLine("Không Tồn tại Máy Tính có ID = " + id);
                Console.WriteLine("Danh sách Máy Tính không thay đổi");

            }

            Console.Write("Bạn có muốn tiếp tục xóa thông tin Máy Tính không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XoaMayTinhTheoID();
            }

        }
        
    }
}
