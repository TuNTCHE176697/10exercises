﻿
using De3.QuanLyMayTinh;
using System.ComponentModel.DataAnnotations;

Console.InputEncoding = System.Text.Encoding.Unicode;
Console.OutputEncoding = System.Text.Encoding.Unicode;

SERVICE service = new SERVICE();
//service.DuLieuBanDau();

while (true)
{
    Console.WriteLine("Chương Trình Quản Lý Máy Tính: ");
    Console.WriteLine("1. Nhập Danh sách Máy Tính");
    Console.WriteLine("2. Xuất Danh sách Máy Tính");
    Console.WriteLine("3. Xóa Máy Tính theo ID Máy Tính");
    Console.WriteLine("4. Xuất Trọng Lượng Máy Tính theo Tên gần đúng");
    Console.WriteLine("0. Thoát");
    Console.Write("Mời bạn nhập lựa chọn: ");
    int choice = KiemTraDauVao.KiemTraLuaChon(0, 4);

    switch (choice)
    {
        case 1:
            service.ThemDanhSachMayTinh();
            break;

        case 2:
            service.XuatDanhSachMayTinh();
            break;

        case 3:
            service.XoaMayTinhTheoID();
            break;

        case 4:
            service.XuatTrongLuongTheoTen();
            break;

        case 0:
            // Thoát khỏi ứng dụng
            Environment.Exit(0);
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ!");
            break;
    }

}
Console.ReadKey();


