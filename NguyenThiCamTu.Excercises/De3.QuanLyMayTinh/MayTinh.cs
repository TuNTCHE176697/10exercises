﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De3.QuanLyMayTinh
{
    internal class MayTinh
    {
        public string ID { get; set; }
        public string Ten { get; set; }
        public float TrongLuong { get; set; }

        public MayTinh() { }
        public MayTinh(string id, string ten, float trongluong)
        {
            this.ID = id;
            this.Ten = ten;
            this.TrongLuong = trongluong;
        }

        public virtual void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-35} {2,-12}", ID, Ten, TrongLuong);
        }
    }
}
