﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace De3.QuanLyMayTinh
{
    internal class KiemTraDauVao
    {
        //Hàm KiemTraLuaChonTiepTuc ép người dùng nhập vào string a hay b
        public static string KiemTraLuaChonTiepTuc(string a, string b)
        {
            while (true)
            {
                string luachon = Console.ReadLine().ToString();
                if (luachon.Equals(a) || luachon.Equals(b) || luachon.Equals(a.ToLower()) || luachon.Equals(b.ToLower()))
                {
                    return luachon;
                }
                else
                {
                    Console.WriteLine("Lựa chọn không hợp lệ! Mời bạn nhập lại!");
                    Console.Write("Mời bạn nhập lựa chọn (" + a + " or " + b + "): ");
                }
            }
        }
        //Hàm KiemTraLuaChon để ép người dùng nhập vào số nguyên trong khoảng yêu cầu
        public static int KiemTraLuaChon(int soNhoNhat, int soLonNhat)
        {
            while (true)
            {
                string luaChon = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(luaChon))
                    {
                        Console.Write("Chưa nhập lựa chọn!\nMời bạn nhập lựa chọn: ");
                    }
                    else
                    {
                        int integerLuaChon = int.Parse(luaChon);
                        if (integerLuaChon < soNhoNhat || integerLuaChon > soLonNhat)
                        {
                            Console.WriteLine("Lựa chọn vượt ngoài phạm vi! Hãy nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat);
                            Console.Write("Nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat + ": ");
                        }
                        else return integerLuaChon;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Lựa chọn không hợp lệ! Hãy nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat);
                    Console.Write("Nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat + ": ");
                }
            }
        }


        //Hàm KiemTraTen dùng để ép người dùng nhập vào chữ cái và số
        public static string KiemTraTen()
        {
            string pattern = @"^[A-Za-zÁ-ỹ0-9\s]+$";
            while (true)
            {
                string ten = Console.ReadLine();
                if (Regex.IsMatch(ten, pattern))
                {
                    return ten;
                }
                else
                {
                    Console.WriteLine("Tên Máy Tính không hợp lệ! Hãy nhập lại Tên Máy Tính");
                    Console.Write("Nhập Tên Máy Tính: ");
                }
            }
        }

        //Hàm KiemTraTrongLuong để ép người dùng nhập vào số float trong khoảng yêu cầu
        public static float KiemTraTrongLuong(float soNhoNhat, float soLonNhat)
        {
            while (true)
            {
                string trongluong = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(trongluong))
                    {
                        Console.Write("Chưa nhập Trọng lượng của Máy Tính!\nHãy nhập lại Trọng lượng của Máy Tính: ");
                    }
                    else
                    {
                        float floatTrongLuong = float.Parse(trongluong);
                        if (floatTrongLuong < soNhoNhat || floatTrongLuong > soLonNhat)
                        {
                            Console.WriteLine("Trọng lượng không hợp lệ! Hãy nhập lại Trọng lượng của Máy Tính!");
                            Console.Write("Nhập Trọng Lượng của Máy Tính: ");
                        }
                        else return floatTrongLuong;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Trọng lượng không hợp lệ! Hãy nhập lại Trọng lượng của Máy Tính!");
                    Console.Write("Nhập Trọng Lượng của Máy Tính: ");
                }
            }
        }
    }
}
