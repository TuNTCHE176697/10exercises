﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De7.QuanLyHocSinh
{
    internal class SERVICE
    {
        List<Student> danhSachHocSinh = new List<Student>();

        ////Hàm DuLieuBanDau để tạo giá trị ban đầu cho danh sách sinh viên
        //public void DuLieuBanDau()
        //{
        //    danhSachHocSinh.Add(new StudentHaveYear { ID = 1, Ten = "Nguyễn Thị Cẩm Tú",Tuoi = 20, NamSinh = 2003, Nganh = "SE" });
        //    danhSachHocSinh.Add(new StudentHaveYear { ID = 2, Ten = "Trương Thị Thanh Hòa", Tuoi = 21, NamSinh = 2002, Nganh = "AI" });
        //    danhSachHocSinh.Add(new StudentHaveYear { ID = 3, Ten = "Trần Thu Trang", Tuoi = 20, NamSinh = 2003, Nganh = "IA" });
        //    danhSachHocSinh.Add(new StudentHaveYear { ID = 4, Ten = "Hồ Việt Hải", Tuoi = 23, NamSinh = 2000, Nganh = "SE" });

        //}

        //Hàm ThemDanhSachHocSinh để cho người dùng nhập vào thông tin học sinh
        public void ThemDanhSachHocSinh()
        {
            Console.WriteLine("Thêm Thông tin Học Sinh:");
            StudentHaveYear hocSinh = new StudentHaveYear();
            var dem = 0;
            Console.Write("Nhập ID Học Sinh: ");
            int ma = 0;
            bool a = true;
            while(a)
            {
                ma = KiemTraDauVao.KiemTraLuaChon(1,int.MaxValue);
                int count = (from hs in danhSachHocSinh
                            where hs.ID == ma
                            select hs).Count();
                if(count > 0 )
                {
                    Console.WriteLine("ID Học Sinh đã tồn tại! Xin hãy nhập lại!");
                    Console.Write("Nhập ID Học Sinh: ");
                }
                else
                {
                    a= false;
                }
            }
            hocSinh.ID = ma;
            Console.Write("Nhập Tên Học Sinh: ");
            hocSinh.Ten = KiemTraDauVao.KiemTraTen("Tên Học Sinh");
            Console.Write("Nhập Ngành Học Sinh");
            hocSinh.Nganh = KiemTraDauVao.KiemTraTen("Tên Ngành");
            Console.Write("Nhập Năm sinh Học Sinh: ");
            int namSinh = KiemTraDauVao.KiemTraNamSinh();
            hocSinh.NamSinh = namSinh;
            hocSinh.Tuoi = (DateTime.Now.Year - namSinh);
            danhSachHocSinh.Add(hocSinh);

            Console.Write("Bạn có muốn tiếp tục nhập thông tin Học Sinh không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                ThemDanhSachHocSinh();
            }
        }

        //Hàm XuatDanhSachHocSinh để xuất ra danh sách sinh viên hiện có với không có năm sinh
        public void XuatDanhSachHocSinh()
        {
            var ketqua = from sv in danhSachHocSinh
                         select sv;
            Console.WriteLine("Danh sách Học Sinh: ");
            Console.WriteLine("Số lượng Học Sinh: " + ketqua.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15}", "ID Học Sinh", "Tên Học Sinh", "Tuổi","Ngành học");
            foreach (Student sv in ketqua)
            {
                sv.inThongTin();
            }

        }

        //Hàm XuatDanhSachHocSinhCoNamSinh để xuất ra danh sách học sinh hiện có với có thông tin năm sinh
        public void XuatDanhSachHocSinhCoNamSinh()
        {
            var ketqua = from sv in danhSachHocSinh
                         select sv;
            Console.WriteLine("Danh sách Học Sinh: ");
            Console.WriteLine("Số lượng Học Sinh: " + ketqua.Count());
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15} {4,-20}", "Mã Học Sinh", "Tên Học Sinh", "Tuổi","Ngành học","Năm Sinh");
            foreach (StudentHaveYear sv in ketqua)
            {
                sv.inThongTin();
            }

        }

        //Hàm XoaHocSinhTheoID cho phép người dùng xóa học sinh theo id học sinh
        public void XoaHocSinhTheoID()
        {
            Console.WriteLine("Xóa Thông tin Học Sinh theo ID Học Sinh:");
            Console.Write("Nhập ID Học Sinh: ");
            int id = KiemTraDauVao.KiemTraLuaChon(1, int.MaxValue);
            var ketqua = from hs in danhSachHocSinh
                         where hs.ID == id
                         select hs;
            if (ketqua.Count() > 0)
            {
                Console.WriteLine("Thông tin Học Sinh có Mã Học Sinh = " + ketqua.SingleOrDefault().ID + " đã được xóa");
                danhSachHocSinh.Remove(ketqua.SingleOrDefault());
                Console.WriteLine("Danh sách Học Sinh sau khi xóa: ");
                XuatDanhSachHocSinhCoNamSinh();
            }
            else
            {
                Console.WriteLine("Không Tồn tại Học Sinh có ID = " + id);
                Console.WriteLine("Danh sách Học Sinh không thay đổi");

            }

            Console.Write("Bạn có muốn tiếp tục xóa thông tin Học Sinh theo ID Học Sinh không?(Có/Không): ");
            string luachon = KiemTraDauVao.KiemTraLuaChonTiepTuc("Có", "Không");
            if (luachon.ToLower().Equals("có"))
            {
                XoaHocSinhTheoID();
            }

        }

    }

}
