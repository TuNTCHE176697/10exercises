﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De7.QuanLyHocSinh
{
    internal class Student
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }

        public string Nganh { get; set; }

        public Student () { 
        }

        public Student(int id, string ten, int tuoi, string nganh)
        {
            this.ID = id;
            this.Ten = ten;
            this.Tuoi = tuoi;
            this.Nganh = nganh;
        }

        public void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15}", ID, Ten, Tuoi, Nganh);
        }
    }

}
