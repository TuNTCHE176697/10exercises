﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace De7.QuanLyHocSinh
{
    internal class StudentHaveYear : Student
    {
        public double NamSinh { get; set; }

        public StudentHaveYear() { 
        }
        public StudentHaveYear(int id, string ten, int tuoi, string nganh, int namsinh) : base(id, ten, tuoi,nganh)
        {
            this.NamSinh = namsinh;
        }

        public void inThongTin()
        {
            Console.WriteLine("{0,-15} {1,-20} {2,-10} {3,-15} {4,-20}", ID, Ten, Tuoi, Nganh, NamSinh);
        }
    }
}
