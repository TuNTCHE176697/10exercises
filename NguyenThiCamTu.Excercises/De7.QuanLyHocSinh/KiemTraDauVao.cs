﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace De7.QuanLyHocSinh
{
    internal class KiemTraDauVao
    {
        //Hàm KiemTraLuaChonTiepTuc ép người dùng nhập vào string a hay b
        public static string KiemTraLuaChonTiepTuc(string a, string b)
        {
            while (true)
            {
                string luachon = Console.ReadLine().ToString();
                if (luachon.Equals(a) || luachon.Equals(b) || luachon.Equals(a.ToLower()) || luachon.Equals(b.ToLower()))
                {
                    return luachon;
                }
                else
                {
                    Console.WriteLine("Lựa chọn không hợp lệ! Mời bạn nhập lại!");
                    Console.Write("Mời bạn nhập lựa chọn (" + a + " or " + b + "): ");
                }
            }
        }
        //Hàm KiemTraLuaChon để ép người dùng nhập vào số nguyên trong khoảng yêu cầu
        public static int KiemTraLuaChon(int soNhoNhat, int soLonNhat)
        {
            while (true)
            {
                string luaChon = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(luaChon))
                    {
                        Console.Write("Chưa nhập lựa chọn!\nMời bạn nhập lựa chọn: ");
                    }
                    else
                    {
                        int integerLuaChon = int.Parse(luaChon);
                        if (integerLuaChon < soNhoNhat || integerLuaChon > soLonNhat)
                        {
                            Console.WriteLine("Lựa chọn vượt ngoài phạm vi! Hãy nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat);
                            Console.Write("Nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat + ": ");
                        }
                        else return integerLuaChon;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Lựa chọn không hợp lệ! Hãy nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat);
                    Console.Write("Nhập số nguyên trong khoảng " + soNhoNhat + " và " + soLonNhat + ": ");
                }
            }
        }

        


        //Hàm KiemTraMa dùng để ép người dùng nhập vào chữ cái và số
        public static string KiemTraMa()
        {
            string pattern = @"^[A-Za-zÁ-ỹ0-9\s]+$";
            while (true)
            {
                string ma = Console.ReadLine();
                if (Regex.IsMatch(ma, pattern))
                {
                    return ma;
                }
                else
                {
                    Console.WriteLine("Mã Học Sinh không hợp lệ! Hãy nhập lại Mã Học Sinh");
                    Console.Write("Nhập Mã Học Sinh: ");
                }
            }
        }
        //Hàm KiemTraTen dùng để ép người dùng nhập vào chữ cái 
        public static string KiemTraTen(string a)
        {
            string pattern = @"^[A-Za-zÁ-ỹ\s]+$";
            while (true)
            {
                string ten = Console.ReadLine();
                if (Regex.IsMatch(ten, pattern))
                {
                    return ten;
                }
                else
                {
                    Console.WriteLine(a+" không hợp lệ! Hãy nhập lại "+a);
                    Console.Write("Nhập "+a+" : ");
                }
            }
        }

        //Hàm KiemTraNamSinh dùng để ép người dùng nhập vào năm sinh không được lớn hơn năm hiện tại
        public static int KiemTraNamSinh()
        {
            while (true)
            {
                string nam = Console.ReadLine();
                try
                {
                    if (string.IsNullOrEmpty(nam))
                    {
                        Console.Write("Chưa điền Năm sinh của Học Sinh!\nMời bạn nhập Năm Sinh của Học Sinh: ");
                    }
                    else
                    {
                        int integerNam = int.Parse(nam);
                        if (DateTime.Now.Year <= integerNam)
                        {
                            Console.WriteLine("Năm sinh không hợp lệ! Xin hãy nhập lại!");
                            Console.Write("Nhập Năm sinh Học Sinh: ");
                        }
                        else return integerNam;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Năm sinh không hợp lệ! Xin hãy nhập lại!");
                    Console.Write("Nhập Năm sinh Học Sinh: ");
                }
            }
        }

    }
}
